function niiToVMR(path, envfile, niiFile) 
%%  from nii to VMR
% Takes in path, envelope VMR file and niiFile, will output with the name
% of the nii file

%% Get ACPC anatomical to use as an envelope
vmr_env = xff([envfile]);

%% We need to convert back. Easy-peasy
% load the nifti files
nii_file = xff([path, niiFile]);

% convert to 8-bit data
temp = double(nii_file.VoxelData);
temp=flip(temp,1);
temp=flip(temp,2);
temp=flip(temp,3);
temp=squeeze(shiftdim(temp,1));

% Set offset to 0 since this is the value for nii files
vmr_env.OffsetX = 0;
vmr_env.OffsetY = 0;
vmr_env.OffsetZ = 0;

% replace image data with the data in niiFile
vmr_env.VMRData16 = [];
vmr_env.VMRData = temp;
vmr_env.SaveAs([path, niiFile(1:end-7),'.vmr']);

