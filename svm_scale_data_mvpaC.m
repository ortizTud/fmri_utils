function [train,test]=svm_scale_data_mvpaC(train, test, flag)
%% Takes in data and normalize it for SVM classification
% flag = 1, normalize voxels (colums) of train, use same params to
%           normalize test data (-1 to 1)
% flag = 2, normalize each pattern itself (mean zero, sd =1)
% flag = 3, zscore (mean zero, sd 1) voxels (colums) of train, use same params
%           to zscore test data
% Author: FWS UWO 12/9/11
% Updated by JOT Goethe 21/06/19

%%
if flag==1     % put training data on -1 to 1 scale

    outTr=zeros(size(train));
    outTe=zeros(size(test));
    
    for i=1:size(train,2)
        tmp=train(:,i);
        p(i,1)=min(tmp);
        p(i,2)=max(tmp);
        h=(tmp-p(i,1))./(p(i,2)-p(i,1)); % put 0 -1
        h=h.*2 - 1; %% put on -1 to 1 scale
        
        outTr(:,i)=h;    % tr data -1 to 1
        
        tmp2=test(:,i);
        h2=(tmp2-p(i,1))./(p(i,2)-p(i,1)); % put 0 -1
        h2=h2.*2 - 1;
        
        outTe(:,i)=h2;   % te data -1 to 1
        
    end
    
    train=outTr;
    test=outTe;
    
elseif flag==2
    
    % zscore across features. ie per example (to have mean zero, sd 1)
    % ie remove any univariate effects
    train=zscore(train')';
    test=zscore(test')';
elseif flag==3
    
    % z score by features
    m=mean(train);  %% feature means
    s=std(train);  %% feature SDs
    m2=repmat(m,size(train,1),1);
    s2=repmat(s,size(train,1),1);
    m3=repmat(m,size(test,1),1);
    s3=repmat(s,size(test,1),1);
    
    train=(train-m2)./s2;   %% z score training matrix
    test=(test-m3)./s3;  %% z scored by train parameters
    
end

