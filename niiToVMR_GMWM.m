function niiToVMR_GMWM(which_sub, envfile, GMfile, WMfile) 
%%  from nii to VMR WM_GM segmentations
%  .nii.gz files, and back again...

% Get sub code
if which_sub<10
    sub_code=['sub-0', num2str(which_sub)];
else
    sub_code=['sub-', num2str(which_sub)];
end

% Get files paths
output_folder=['/home/javier/pepe/2_Analysis_Folder/PIVOTAL/FeedBES/BV_analyses/', sub_code,'/anat/'];
input_folder=['/home/javier/pepe/2_Analysis_Folder/PIVOTAL/FeedBES/ITK_segmentations/', sub_code,'/'];
input_folder=''
output_folder=''
% Get output names
output_prefix=envfile(1:end-4);

%% Get ACPC anatomical to use as an envelope
vmr_env = xff([output_folder, envfile]);

%% We need to convert back. Easy-peasy
% load the nifti files
gm_seg = xff([input_folder, GMfile]);
wm_seg = xff([input_folder, WMfile]);

% convert to 8-bit data (150 and 100 values for BV meshes)
gm_msk = uint8(double(gm_seg.VoxelData).*100);
% gm_msk= flipdim(gm_msk,1);
% gm_msk= flipdim(gm_msk,2);
% gm_msk= flipdim(gm_msk,3);
% gm_msk=squeeze(shiftdim(gm_msk,2));

wm_msk = uint8(double(wm_seg.VoxelData).*240);
% wm_msk= flipdim(wm_msk,1);
% wm_msk= flipdim(wm_msk,2);
% wm_msk= flipdim(wm_msk,3);
% wm_msk=squeeze(shiftdim(wm_msk,2));

% merge both masks
loc=find(wm_msk>1);
gm_msk(loc) =150;

% replace image data with the new segmentations
% vmr_env.VMRData16 = [];
vmr_env.VMRData = gm_msk;
vmr_env.SaveAs([output_folder, output_prefix,'_segmented.vmr']);

% output the wm mask only as well
vmr_env.VMRData = wm_msk;
vmr_env.SaveAs([output_folder, output_prefix,'_WM_only.vmr']);


