function nii2vmr(path)
%% Convert nii files from BIDS structure into VMR

% Creat neuroElf object
netools=neuroelf;
nSubs = 10;

for cSub=1:nSubs
    % List files
    if cSub<10
        subDir=([path '\sub-0' num2str(cSub)]);
    else
        subDir=([path '\sub-' num2str(cSub)]);
    end
    
    % List anatomical
    niiF=dir([subDir '\anat\*.nii']);
    gzF=dir([subDir '\anat\*.nii.gz']);
    fileList=[niiF;gzF];
    
    for i=1:length(fileList)
        % Get file name
        fileName=[subDir '\anat\' fileList(i).name];
        
        % Import nifti file
        %     fmr = importfmrfromanalyze(imgs, fileName, [], 3000, []);
        
        % Import anatomical files
        vmr=netools.importvmrfromanalyze(fileName);
        
        % Save vmr
        vmr.SaveAs([fileName(1:end-4) '.vmr']);
    end
    
    % Print status
    ['Sub ' num2str(cSub) ' completed']
end

%% FMR
fmr=xff('sub-01_task-restingbaseline_bold.fmr');
nii = xff('sub-01_task-restingbaseline_bold.nii.gz');
nii.SaveAs('testFMR.nii')
