function vmr2nii(path,vmrFile,niiFile)
% Convert VMR files to nii files
% Takes in: path,vmrFile,niiFile.
% Needs NeuroElf
% addpath(genpath('X:\Memory-driven_predictions\scripts\NeuroElf_v11_7251'))
% addpath(genpath('/analyse/Project0163/Memory-driven_predictions/scripts/NeuroElf_v11_7251'))

%% VMR to nii
% VMR to use
vmr=xff([path,vmrFile]);

% Save nii file
vmr.ExportNifti([path,niiFile])
end