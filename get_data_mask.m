function [masked_data]=get_data_mask(data_file, mask_file, threshold, output_format)
%% Read in volume data
% This script imports data from data_file (NIFTI format) and applies mask_file.
%
% If data_file is 4D (i.e., usually functional files), mask_data outputs
% the timecourse of each voxel in mask_file as voxel X time matrix. Voxel
% indeces are stored in a separate variable.
% If data_file is 3D (i.e., usually beta maps), mask_data outputs a 3D
% matrix or a vector array based on output_format (1 for 3D matrix, 2 for
% vector).
%
% mask_file can be either a binary mask or a contrast map.
%
% threshold should be set to 1 for binary masks or to the desired
% threshold for contrast maps.
plot_mask=0;
% output_format still not done

%% Read in mask_file

roi_mask=xff(mask_file);
roi_mask=roi_mask.GetVolume(1);

% Check if mask is binary
if numel(unique(roi_mask))==2
    isbinary=1;
else
    isbinary=0;
end

% Convert into logicals for easier use
[r,c,v] = ind2sub(size(roi_mask),find(roi_mask >= threshold));
roi_mask = roi_mask >= threshold;
roi_mask = find(roi_mask);

%% Read in data_file

% State
fprintf('Loading: %s\n', data_file);
data = xff(data_file);
% data=data.VoxelData;

% Check whether 3D or 4D
if size(data.VoxelData,4)==1
    is3D=1;
else
    is3D=0;
end

%% Extract data
if is3D==0 % If dealing with 4D data (i.e., functional files)
    %State
    fprintf('4D file found. Extracting timecourse\n');
    
    % Get info from data
    nVox=length(roi_mask);
    nVols=size(data.VoxelData,4);
    
    % Pre-allocate
    data_out=zeros(nVols,nVox);
    
    % Get timecourse of each voxel in mask_file (and only of those)
    for i=1:nVols
        tmp=[];
        tmp=data.GetVolume(i);  %% one volume all voxels
        data_out(i,:)=tmp(roi_mask);
        
        % Print state
        if mod(i,100)==1
            fprintf('%d percent %d\n\n', floor(i/nVols*100))
        end
    end
    
    % Do you want to see the mask over the data? TO DO.
    if plot_mask == 1
        plotROIoverFuncData(sufs,cSub,tmp,roi_mask,roi_name);
    end
    
    % Check for zeros in data.
    if length(find(data_out(:,:)==0))>0
        warning('Zeros in data: this might require some filtering!');
    end
    
    % State
    disp('Extraction finished.')
    
elseif is3D==1 % If dealing with 3D data (i.e., contrast map)
    
    %State
    fprintf('3D file found. Extracting values\n');
    
    % Get info from data
    nVox=length(roi_mask);
    
    % Pre-allocate
    data_out=zeros(nVox,1);
    
    % Get values of each voxel in mask_file (and only of those)
    tmp=[];
    tmp=data.GetVolume(1);  %% one volume all voxels
    data_out=tmp(roi_mask);
    
    % Do you want to see the mask over the data? TO DO.
    if plot_mask == 1
        plotROIoverFuncData(sufs,cSub,tmp,roi_mask,roi_name);
    end
    
    % Check for zeros in data.
    if length(find(data_out(:,:)==0))>0
        warning('Zeros in data: this might require some filtering!');
    end
    
    % State
    disp('Extraction finished.')
end

masked_data.data_out=data_out;
masked_data.ind.r=r;
masked_data.ind.c=c;
masked_data.ind.v=v;
